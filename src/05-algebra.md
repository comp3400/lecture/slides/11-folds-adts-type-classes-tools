## Algebra { data-transition="fade none" }

<div style="font-size:xx-large">
  Yes &hellip; algebra :)
</div>

---

## Algebra

<div style="font-size:xx-large">
  Remember?
</div>

<pre style="background-color: black; color: white">
x + 4 = 9
&#8756; x = 5

c * 7 = 14
&#8756; c = 2
</pre>

---

## Algebra

* We have seen these things called *Algebraic Data Types*
* However, what is so algebraic about them?
* Where is the algebra?

---

## Algebra


<div style="font-size:xx-large">
  a `BibblyBloo` is a `Bool` AND a `Bool` AND a `Bool`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BibblyBloo =
  BibblyBloo
    Bool Bool Bool
</code></pre>

<hr>

<div style="font-size:xx-large">
  **How many different `BibblyBloo` values are there?**
</div>

---

## Algebra

* There are exactly `8` possible `BibblyBloo` values
* Also, there are `2 * 2 * 2` possible values

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BibblyBloo =
  BibblyBloo
    Bool Bool Bool
</code></pre>

---

## Algebra

* Specifically, there are `Bool * Bool * Bool` values
* This is a *product type*
* It is the product of three `Bool` values
* The give-away for multiplication was the use of the word "AND"
* "`Bool` AND `Bool` AND `Bool`"

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BibblyBloo =
  BibblyBloo
    Bool Bool Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  But why is `Bool = 2`?
</div>

---

## Algebra

* Let's start with some building blocks
* `Void = 0`
* `Unit = 1`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Void
data Unit = Unit
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  Well, here is `Bool`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bool = False | True
</code></pre>

---

## Algebra

* Data type constructors can accept `Unit` without changing structure
* This is still `Bool`
* `Unit` is an identity on constructor arguments
* Well, of course, because they are a *product*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bool = False Unit | True Unit
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  But what does this mean algebraically?
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bool = False Unit <mark>|</mark> True Unit
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  `Bool` is "`Unit` OR `Unit`"
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bool = False <mark>Unit</mark> <mark>|</mark> True <mark>Unit</mark>
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  `Bool` is "`1` OR `1`"
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bool = False <mark>Unit</mark> <mark>|</mark> True <mark>Unit</mark>
</code></pre>

---

## Algebra

* Just like "AND" corresponds to a **product** &hellip;
* "OR" corresponds to **sum**
* `Bool = Unit + Unit = 2`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Bool = False Unit | True Unit
</code></pre>

---

## Algebra

* This leads us to a vocabulary
* We may talk about "product types"
* Or we may talk about "sum types"
* We are talking specifically about algebraic structure of a data type

---

## Algebra

<div style="font-size:xx-large">
  Let's do a slightly less trivial example
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B | C Bool Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A <mark>Bool</mark> | B | C Bool Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2 +` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool <mark>|</mark> B | C Bool Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2 + 1` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | <mark>B</mark> | C Bool Bool
</code></pre>

<hr>

<div style="font-size:24px">
  Recall: a constructor with no arguments &#8594; `Unit`
</div>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2 + 1 +` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B <mark>|</mark> C Bool Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2 + 1 + 2` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B | C <mark>Bool</mark> Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2 + 1 + 2 *` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B | C Bool<mark> </mark>Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `2 + 1 + 2 * 2`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B | C Bool <mark>Bool</mark>
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `BlipBlop` is `7`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B | C Bool Bool
</code></pre>

---

## Algebra

<pre><code class="language-haskell hljs" data-trim data-noescape>
data BlipBlop =
  A Bool | B | C Bool Bool

-- there are seven possible values of the type BlipBlop
-- write them!
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  What about type variables?
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Blap a =
  Blap a Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  `Blap a` is `a * Bool`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Blap a =
  Blap <mark>a Bool</mark>
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  Type variables are algebraic equations with free variables

  `Blap a` = `a * Bool`
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Blap a =
  Blap a Bool
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  What about recursion?
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `List a` is &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `List a` is `1` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = <mark>Nil</mark> | Cons a (List a)
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `List a` is `1 +` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil <mark>|</mark> Cons a (List a)
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `List a` is `1 + a` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons <mark>a</mark> (List a)
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `List a` is `1 + a *` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a<mark> </mark>(List a)
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  A `List a` is `1 + a * ?` &hellip;
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a <mark>(List a)</mark>
</code></pre>

---

## Algebra

<div style="font-size:xx-large">
  Algebraic rules apply!
</div>

---

## Algebra

<pre style="background-color: black; color: white">
List a = 1 + a * List a
       = 1 + a * (1 + a * List a)
       = 1 + a + a&#178; * List a
       = 1 + a + a&#178; * (1 + a * List a)
       = 1 + a + a&#178; + a&#179; * List a
       &hellip;
       = 1 + a + a&#178; + a&#179; + a&#x2074; + &hellip;
</pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)
</code></pre>

---

## Algebra

<pre style="background-color: black; color: white; text-align: center">

List a = 1 + a + a&#178; + a&#179; + a&#x2074; + &hellip;

</pre>

<hr>

<div style="font-size:x-large">
  A list of `a` is "1 OR a OR (a * a) OR (a * a * a) OR (a * a * a * a) OR &hellip;"
</div>


---

## Algebra

###### Distributive Law

<pre style="background-color: black; color: white; text-align: center">

a * (b + c) = a * b + a * c

</pre>

---

## Algebra

###### Distributive Law

<pre style="background-color: black; color: white; text-align: center">

a * (b + c) = a * b + a * c

</pre>

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Addition a b = Add1 a | Add2 b
data Multiplication a b = Multiplication a b
</code></pre>

---

## Algebra

###### Distributive Law

<pre style="background-color: black; color: white; text-align: center">

<mark>a * (b + c)</mark> = a * b + a * c

</pre>

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Addition a b = Add1 a | Add2 b
data Multiplication a b = Multiplication a b

data LeftSide a b c =
  LeftSide a (Addition b c)
</code></pre>

---

## Algebra

###### Distributive Law

<pre style="background-color: black; color: white; text-align: center">

a * (b + c) = <mark>a * b + a * c</mark>

</pre>

<hr>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Addition a b = Add1 a | Add2 b
data Multiplication a b = Multiplication a b

data LeftSide a b c =
  LeftSide a (Addition b c)

data RightSide a b c =
  RightSide
    (Addition (Multiplication a b) (Multiplication a c))
</code></pre>

---

## Algebra

###### Distributive Law

* These two data types have exactly the same structure
* They are isomorphic *(same form)*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Addition a b = Add1 a | Add2 b
data Multiplication a b = Multiplication a b

data LeftSide a b c =
  LeftSide a (Addition b c)

data RightSide a b c =
  RightSide
    (Addition (Multiplication a b) (Multiplication a c))
</code></pre>

---

## Algebra

###### Exercise

<div style="font-size:large">
  Substitute types (e.g. `Bool`, `Unit`) for `a`, `b` and `c` and count all the values that you can make. Are they the same in both cases?
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Addition a b = Add1 a | Add2 b
data Multiplication a b = Multiplication a b

data LeftSide a b c =
  LeftSide a (Addition b c)

data RightSide a b c =
  RightSide
    (Addition (Multiplication a b) (Multiplication a c))
</code></pre>

---

## Algebra

###### Fun Side-exercise

* This is just a fun exercise
* The details and degree of difficulty of this exercise are **NOT** covered in COMP3400
* You will not be assessed on an exercise of this type
* But it's fun, and good practice too :)

---

## Algebra

###### Fun Side-exercise

1. Take any parameterised data type, such as `List`
2. Convert it to its algebraic representation
3. Take the *partial derivative* of the expression
4. Convert that result back to a data type
5. Reflect on the resulting data type
6. What does this new data type do?

---

## Algebra

###### Exponentiation

* We have seen addition (sum type) and multiplication (product type)
* What about exponentiation?
* We can multiply two data types
* But can we take the exponent of a data type to another data type?

---

## Algebra

###### Exponentiation

* Yep!
* That is exactly what `(->)` does

<pre style="background-color: black; color: white; text-align: center">

A -> B
B&#7468;

</pre>

---

## Algebra

###### Exponentiation

<pre><code class="language-haskell hljs" data-trim data-noescape>
Bool -> Unit -- 1&#178;
Unit -> Bool -- 2&#185;
Bool -> Bool -- 2&#178;
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

A -> B
B&#7468;

</pre>

---

## Algebra

###### Power of a Power Law

<pre style="background-color: black; color: white; text-align: center">

(C&#7470;)&#7468;

=

C&#8317;&#7470;&#735;&#7468;&#8318;

</pre>

---

## Algebra

###### Power of a Power Law

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
B -> C
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

<mark>(C&#7470;)</mark>&#7468;

=

C&#8317;&#7470;&#735;&#7468;&#8318;

</pre>

---

## Algebra

###### Power of a Power Law

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
A -> B -> C
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

<mark>(C&#7470;)&#7468;</mark>

=

C&#8317;&#7470;&#735;&#7468;&#8318;

</pre>

---

## Algebra

###### Power of a Power Law

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
A -> B -> C

(A,B)
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

(C&#7470;)&#7468;

=

C&#8317;<mark>&#7470;&#735;&#7468;</mark>&#8318;

</pre>

---

## Algebra

###### Power of a Power Law

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
A -> B -> C

(A,B) -> C
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

(C&#7470;)&#7468;

=

<mark>C&#8317;&#7470;&#735;&#7468;&#8318;</mark>

</pre>

---

## Algebra

###### Power of a Power Law

* These two types have the same structure
* They are isomorphic
* This is *"the curry/uncurry isomorphism"*
* The uncurried form looks like a two argument function
* It's a single argument function taking a product

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
A -> B -> C

(A,B) -> C
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

(C&#7470;)&#7468;

=

C&#8317;&#7470;&#735;&#7468;&#8318;

</pre>

---

## Algebra

<div style="font-size:xx-large">
  Does all this algebra have a practical purpose?
</div>

---

## Algebra

* If you can structure a problem algebraically, you can exploit equivalences
* You can find errors in your algebra that correspond to how you model your program (data types)
* If you can describe part of your problem, you can use algebra to discover the rest
* Clients in the real world don't tell you problems using algebra :)
* However, you can algebraically describe and understand your resulting structure
* **Understanding the algebra of program structure is a useful problem-solving tool**

---

## Algebra

###### Exponentiation &mdash; Exercise

<div style="font-size:large">
  How many functions with this type are there?
</div>

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
Addition Bool Unit -> Bool
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

A -> B
B&#7468;

</pre>

---

## Algebra

###### Exponentiation &mdash; Exercise

<div style="font-size:large">
  There are 2&#8317;&#178; &#8314; &#178;&#8318; functions with this type. Write them all!
</div>

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
Addition Bool Bool -> Bool
</code></pre>

<pre style="background-color: black; color: white; text-align: center">

A -> B
B&#7468;

</pre>

---

## Algebra

###### Exercise

<div style="font-size:x-large">
  Describe `Optional` algebraically
</div>

<pre style="text-align:center"><code class="language-haskell hljs" data-trim data-noescape>
data Optional a = Empty | Full a
</code></pre>

