## Immutable Data Types { data-transition="fade none" }

---

## Immutable Data Types

###### Suppose the following data types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Vehicle =
  Vehicle
    String -- make
    VehicleRegistration

data VehicleRegistration =
  VehicleRegistration
    String -- reg number
    Person -- owner

data Person =
  Person
    String -- name
    Int    -- age
</code></pre>

---

## Immutable Data Types

###### Use-case: *"the owner of the vehicle has had a birthday"*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Vehicle =
  Vehicle
    String -- make
    VehicleRegistration

data VehicleRegistration =
  VehicleRegistration
    String -- reg number
    Person -- owner

data Person =
  Person
    String -- name
    Int    -- age
</code></pre>

---

## Immutable Data Types

###### We write a `birthday` function

* This is *very clumsy* code
* It might tempt the use of an updating variable
  * `v.reg.own.age += 1`

<pre><code class="language-haskell hljs" data-trim data-noescape>
birthday v = case v of
  Vehicle mk reg ->
    case reg of
      VehicleRegistration num own ->
        case own of
          Person nm age ->
            Vehicle mk (
              VehicleRegistration num (
                Person nm (
                  age+1
                )
              )
            )
</code></pre>

---

## Immutable Data Types

###### This problem has several variations

* Update *all of the vehicles* with the owner's birthday
* The earlier problem had a 1:1 mapping
  * one `Vehicle` has exactly one `VehicleRegistration`
  * one `VehicleRegistration` has exactly one `Person` owner
  * one `Person` has exactly one `Int` age
* In this case, we have a 1:many mapping
  * one `VehicleDatabase` has zero or more `Vehicle`
  
<pre><code class="language-haskell hljs" data-trim data-noescape>
data VehicleDatabase =
  VehicleDatabase
    [Vehicle]
</code></pre>

---

## Immutable Data Types

###### This problem has several variations

* In this case, an `Age` has a different mapping
* an `Age` has an `Int` **or** something else
  
<pre><code class="language-haskell hljs" data-trim data-noescape>
data Age =
  Years Int
  | DateOfBirth Int Int Int
</code></pre>

---

## Immutable Data Types

###### This problem of "updating immutable data types"

* We are going to learn about approaches to abstracting this general problem
* The goal today is only: to understand the problem (not the solution)
* A solution will involve learning several new data types and type classes

