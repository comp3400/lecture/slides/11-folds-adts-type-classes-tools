## Variance { data-transition="fade none" }

---

## Variance

###### Covariance

* We have already seen *covariant functors*
* For example:
  * `List`
  * `Optional`
  * `((->) t)`
* *"if I can get from A to B, then I can get from List A to List B"*
* The operation that performs covariant mapping is called `fmap`

<pre><code class="language-haskell hljs" data-trim data-noescape>
fmap :: Functor k => (a -> b) -> k a -> k b
</code></pre>

---

## Variance

###### Covariance

* A covariant functor can be identified when the value over which it is used appears *only in positive position*
* Positive position means: appears in a result (or output)
* Consider the following data types

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Vector3 a = Vector3 a a a

data Function2 a b = Function2 (a -> a -> b)
</code></pre>

---

## Variance

###### Covariance

* `Vector3` is defined over a type-variable `(a)`
* In the definition of `Vector3 a`, the `(a)` value appears only in positive position
* `Function2 a` is defined over a type-variable `(b)`
* In the definition of `Function2 a b`, the `(b)` value appears only in positive position
* These are *covariant functors*

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Vector3 <mark>a</mark> = Vector3 <mark>a</mark> <mark>a</mark> <mark>a</mark>

data Function2 a <mark>b</mark> = Function2 (a -> a -> <mark>b</mark>)

instance Functor Vector3 where -- todo
instance Functor (Function2 a) where -- todo
</code></pre>

---

## Variance

###### Contravariance

* Not all functors are covariant
* A functor may be defined over a value that appears *only in negative position*
* Negative position means: appears in an input (or argument)
* In the following example, `Predicate` is defined over a value `(a)`
* This value appears only in negative position

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Predicate a = Predicate (a -> Bool)
</code></pre>

---

## Variance

###### Contravariance

* Functors that are defined over values only in negative position are called **contravariant functors**
* We might consider a type class to denote `Contravariant`
* Note the difference in type to covariant `fmap`
* Contravariant functors satisfy the usual laws of identity and composition

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Contravariant k where
  contramap :: (b -> a) -> k a -> k b

data Predicate a = Predicate (a -> Bool)

-- exercise!
instance Contravariant Predicate where
</code></pre>

---

## Variance

###### Contravariance

* If we look at `(->)`, it has "two" type variables
* The first one is in negative position <pre style="background-color: black; color: white; font-size: x-large">  <mark>a</mark> -> b</pre>
* The second one is in positive position <pre style="background-color: black; color: white; font-size: x-large">  a -> <mark>b</mark></pre>
* This is why `((->) t)` is a covariant functor
* The contravariant functor is in `(->)` after we *flip* the order of its type variables

---

## Variance

###### Contravariance

<div style="font-size:xx-large">
  Practice
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Func2 a b = Func2 (b -> b -> a)

data DoubleFunc2 a b = DoubleFunc2 (Func2 a b) (Func2 a b)

instance Contravariant (Func2 a) where
instance Contravariant (DoubleFunc2 a) where
</code></pre>

---

## Variance

###### Invariance and Bivariance

* There are two other considerations for covariance and contravariance
* Invariant: neither contravariant nor covariant
* Bivariant: both contravariant and covariant

---

## Variance

###### Invariance

* Neither contravariant nor covariant
* Invariant functors are relatively uninteresting
* Sometimes called exponential functors
* Nevertheless, here is an example

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Invariant k where
  xmap :: (a -> b) -> (b -> a) -> k a -> k b

data Endomorphism a = Endomorphism (a -> a)

instance Invariant Endomorphism where
</code></pre>

---

## Variance

###### Bivariance

* Both contravariant and covariant
* This means we can have an instance of both `Contravariant` and `Functor`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Phantom a = Phantom

instance Functor Phantom where
instance Contravariant Phantom where
</code></pre>

---

## Variance

###### Summary

* Everything we have seen so far is relates to unary functors
* That is, those defined over a single type variable or kind `(* -> *)`
* Unary functors can be one of:
  * contravariant
  * covariant
  * both
  * neither

---

## Variance

###### Other functors

* Consider the following data type
* It is covariant in both its values

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Double a b = Double a a b b
</code></pre>

---

## Variance

###### Other functors

* Specifically, we could map (covariantly) over it on both sides:

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Double a b = Double a a b b

mapA :: (a -> x) -> Double a b -> Double x b
mapB :: (b -> x) -> Double a b -> Double a x
</code></pre>

---

## Variance

###### Other functors

* `Double` is a binary functor
* covariant in both positions

<pre style="background-color: black; color: white">
> :kind Double
Double :: * -> * -> *
</pre>

<pre><code class="language-haskell hljs" data-trim data-noescape>
data Double a b = Double a a b b
</code></pre>

---

## Variance

###### Other functors

* Let's write another binary functor
* `ToInt` is a binary functor
* It is contravariant in both of its positions
* We could map (contravariantly) over it on both sides

<pre><code class="language-haskell hljs" data-trim data-noescape>
data ToInt a b = ToInt (a -> Int) (b -> Int)

-- exercise
contramapA :: (x -> a) -> ToInt a b -> ToInt x b
contramapB :: (x -> b) -> ToInt a b -> ToInt a x
</code></pre>

---

## Variance

###### Other functors

* `(->)` is a binary functor
* What is its variance?

<pre style="background-color: black; color: white">
> :kind (->)
(->) :: * -> * -> *
</pre>
