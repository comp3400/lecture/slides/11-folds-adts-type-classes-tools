## Folds { data-transition="fade none" }

* We have talked briefly about list folds in lectures and more in-depth in prac sessions
* Today, we will see an intuition for how we think about folds
* There are two common types of list folds; left and right
* We will discuss each separately and see how this intuition can solve problems

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: (a -> b -> b) -> b -> List a -> b
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* Let's think of `foldr` as a function taking "two arguments"
* We know that "taking two arguments" is an approximation
* The distinction here is irrelevant to developing the intuition

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: <mark>(a -> b -> b)</mark> -> <mark>b</mark> -> List a -> b
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* After the two arguments, it then takes a list argument
* Let's think of `foldr` as taking two arguments and a list

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: (a -> b -> b) -> b -> <mark>List a</mark> -> b
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* In summary, `foldr` takes two arguments, a list, and produces a value
* The return value is of type `b`

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldr :: (a -> b -> b) -> b -> List a -> <mark>b</mark>
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* We also know that all lists are made up of `Cons` or `Nil`
* These are the two constructors for `List`

<pre><code class="language-haskell hljs" data-trim data-noescape>
data List a = Nil | Cons a (List a)

foldr :: (a -> b -> b) -> b -> List a -> b
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* Here is what `foldr` does to the list
* Where there is `Cons`, it is replaced with the first argument
* Where there is `Nil`, it is replaced with the second argument
* Examples are below, including a Haskell list, using infix notation
* Note that the cons operation is *right-associative*

<pre><code class="language-haskell hljs" data-trim data-noescape>
mylist1 = a : b : c : []
mylist2 = Cons a (Cons b (Cons c Nil))

-- &#x2295; denotes an arbitrary operator
foldr &#x2295; z mylist1 == a &#x2295; b &#x2295; c &#x2295; z
foldr func z mylist2 == func a (func b (func c z))
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* `foldr` replaces the constructors of a list with the two arguments
* It does this in *no particular execution order*
* Another example: let's add the numbers in a list

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- replace Cons with (+)
-- replace Nil with 0
-- this will add up the numbers!
addList list = foldr (+) 0 list

-- e.g. addList (Cons 7 (Cons 8 (Cons 9 Nil)))
--
-- was Cons 7 (Cons 8 (Cons 9 Nil))
-- now (+) 7 ((+) 8 ((+) 9 0))
--   = 7 + 8 + 9 + 0
--   = 22     
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* Another example: let's append two lists
* Suppose we have two lists, `x` and `y`
* We wish to append `x` and `y`

<pre><code class="language-haskell hljs" data-trim data-noescape>
appendList :: List a -> List a -> List a
appendList x y = _
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* We wish to append `x` and `y`
* Let's solve this by *replacing constructors in `x`*

<pre><code class="language-haskell hljs" data-trim data-noescape>
appendList :: List a -> List a -> List a
appendList x y = foldr _ _ x
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* We wish to append `x` and `y`
* Specifically, in `x`, we wish to replace:
  * `Cons` with `Cons`
  * `Nil` with `y`
* This will append the two lists!

<pre><code class="language-haskell hljs" data-trim data-noescape>
appendList :: List a -> List a -> List a
appendList x y = foldr Cons y x
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

<pre><code class="language-haskell hljs" data-trim data-noescape>
appendList :: List a -> List a -> List a
appendList x y = foldr Cons y x

-- suppose:
--   x = Cons a (Cons b (Cons c Nil))
--   y = Cons d (Cons e (Cons f Nil))
--
--   appendList x y =
--   Cons a (Cons b (Cons c (Cons d (Cons e (Cons f Nil)))))
</code></pre>

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* Right folds replace constructors in the given list
* Looking at the source code to `foldr`, this may not be immediately evident
* However, this intuition is **precise** &mdash; there are no exceptions to the rule
* If this intuition helps you solve a problem, use it

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* There are some consequences to this definition
* For example, not all lists have `Nil` (infinite lists)
* For an infinte list, the replacement of `Nil` simply never occurs
* Remember, there is no *order of execution*

---

## Folds

##### Right folds *(`foldr` or `foldRight`)*

* Another consequence is a `foldr` identity
* `(foldr Cons Nil x)` means *"leave the constructors alone in `x`"*
* This will always return `x`
* `foldr Cons Nil x = x`

---

## Folds { data-transition="fade none" }

##### Left folds *(`foldl` or `foldLeft`)*

* A left fold is a little messier
* However, drawing on our existing programming knowledge, we can develop a very precise intuition for what it does

<pre><code class="language-haskell hljs" data-trim data-noescape>
foldl :: (b -> a -> b) -> b -> List a -> b
</code></pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<div style="font-size: x-large; text-align: center">
Remember loops?
</div>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<div style="font-size: x-large; text-align: center">
Here is a loop
</div>

<pre style="background-color: black; color: white">
var r = z

for(elem in list) {
  r = func(r, elem)
}

return r
</pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

* Pay particular attention to three parts
  * The initial value before the loop
  * The function that is run on each loop iteration
  * The list itself
* Let's factor them out as arguments

<pre style="background-color: black; color: white">
var r = <mark>z</mark>

for(elem in <mark>list</mark>) {
  r = <mark>func</mark>(r, elem)
}

return r
</pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<pre style="background-color: black; color: white">
\f z list ->
  var r = <mark>z</mark>

  for(elem in <mark>list</mark>) {
    r = <mark>func</mark>(r, elem)
  }

  return r
</pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

* This is what a left fold does
* This intuition is **precise**
* Any question we ask about this loop has the same answer as a left fold

<pre style="background-color: black; color: white">
\f z list ->
  var r = z

  for(elem in list) {
    r = func(r, elem)
  }

  return r
</pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

* Does this loop ever work on an infinite list?
* If I replace `func` with `(+)` and `z` with `0` what does it do?

<pre style="background-color: black; color: white">
\f z list ->
  var r = z

  for(elem in list) {
    r = func(r, elem)
  }

  return r
</pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<div style="font-size: x-large; text-align: center">
A left fold **does exactly that loop**
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- add the numbers of a list
addList list = foldl (\r elem -> r + elem) 0 list
</code></pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<div style="font-size: x-large; text-align: center">
Do this on each iteration of the loop
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- add the numbers of a list
addList list = foldl <mark>(\r elem -> r + elem)</mark> 0 list
</code></pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<div style="font-size: x-large; text-align: center">
Start the loop here
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- add the numbers of a list
addList list = foldl (\r elem -> r + elem) <mark>0</mark> list
</code></pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

<div style="font-size: x-large; text-align: center">
Do the loop on this list
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- add the numbers of a list
addList list = foldl (\r elem -> r + elem) 0 <mark>list</mark>
</code></pre>

---

## Folds

##### Left folds *(`foldl` or `foldLeft`)*

* If we look at the source code to `foldl` this looping is not at all evident
* However, we may view `foldl` as a black box that does this loop

---

## Folds

##### Exercises

<div style="font-size: x-large; text-align: center">
Which fold will you use?
</div>

<div style="font-size: 36px; text-align: center">
<pre><code class="language-haskell hljs" data-trim data-noescape>
-- multiply the numbers in a list
product :: List Int -> Int

-- the length of a list
length :: List a -> Int

-- map a function on list elements
mapList :: (a -> b) -> List a -> List b

-- append two lists together
append :: List a -> List a -> List a

-- flatten a list of lists, to a list
flatten :: List (List a) -> List a

-- reverse a list
reverse :: List a -> List a
</code></pre>
</div>

