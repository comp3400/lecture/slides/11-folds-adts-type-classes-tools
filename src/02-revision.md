## Revision { data-transition="fade none" }

###### The `Semigroup` type-class

* A `semigroup` is any set and closed, binary, associative operation
* For example
  * `Int` and `(+)`
  * `Int` and `(*)`
  * `List a` and `(++)`
  * `a -> a` and `(.)`
* `class Semigroup` is a type class that denotes this structure
* The compiler will **not** enforce associativity &mdash; it is a programmer's responsibility

---

## Revision

###### The `Monoid` type-class

* A `monoid` is any semigroup that has also an identity on the associative operation
* For example
  * `Int` and `(+)` and `0`
  * `Int` and `(*)` and `1`
  * `List a` and `(++)` and `Nil`
  * `a -> a` and `(.)` and `id`
* `class Monoid` is a type class that denotes this structure
* All monoids are semigroups
* Not all semigroups are monoids

---

## Revision

###### The `Monad` type-class

* `do` is a keyword that provides a syntactic notation for the use of `(>>=)`
* An intuition for the use-case for **the reader monad** is
  * a constant value is required throughout the application
  * passing that value explicitly becomes clumsy
  * we wish to write programs *as if it were passed*
  * the reader monad provides a "small library" to achieve this

---

## Revision

###### `Functor` composition

* The composition of two `Functor` instances, is also a `Functor`
* Functors are *closed under composition*
* This is also true of `Applicative`
* It is **not** true of `Monad` *(try it!)*

---

## Revision

###### Monad Transformers

* An approach to composing monads (since they do not compose in general) is **Monad Transformers**
* Monad Transformers compose an arbitrary monad with a *specific* monad
* Colloquially, we call this the "X monad transformer" for the specific monad X
* Conventionally, the name of the monad transformer is the specific monad followed by `T`
* For example, `data OptionalT` is the name for "the Optional monad transformer"

---

## Revision

* We can recover the original specific monad, from its transformer, with `Identity`
* `Identity` is a monad that intuitively, "does nothing"
* `data State s a` is a data type that intuitively, simulates modifying a value
* `State s` has a `Monad` instance
* `StateT s k` is the monad transformer for `State`

---

## Revision

###### The `traverse` and `sequence` functions

* `traverse` is a function that is *very ubiquitous*
* This is because it solves so many common problems as we saw
* `traverse` is often interchanged with another function, `sequence`
* `traverse` and `sequence` have *equal power* &mdash; one can be derived from the other
* The original paper on this function is *Gibbons, Oliveira (2006). The Essence of the Iterator Pattern*

---

## Revision

<div style="text-align: center; font-size: x-large">
  The ubiquity of <code>traverse</code> is a regular source of light comedy
</div>

<div style="text-align:center">
  <a href="images/runar-twitter-traverse.png">
    <img src="images/runar-twitter-traverse.png" alt="Runar on Twitter on traverse" style="width:640px"/>
  </a>
</div>

---

## Revision

###### The `Foldable` and `Traversable` type classes

* `Foldable` is a type class that generalises folds
* `Foldable` is closed under composition
* `Traversable` is a type class that is a subclass of `Foldable` and `Functor`
* `Traversable` generalises `traverse` on the container
* We were left with a question, *"is Traversable closed under composition?"*

