## Foldable { data-transition="fade none" }

* The `Foldable` type class generalises folds on the container type `(k)`
* It has many instances e.g.
  * `[]`
  * `Maybe`
  * `((,) a)`
* You may see it defined with `foldMap` &mdash; a function that is equal in power to `foldr`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Foldable k where
  foldr :: (a -> b -> b) -> b -> k a -> b
</code></pre>

---

## Traversable { data-transition="fade none" }

* The `Traversable` type class is a subclass of both `Foldable` and `Functor`
* It also has many instances
* You may see it defined with `sequence` &mdash; a function that is equal in power to `traverse`

<pre><code class="language-haskell hljs" data-trim data-noescape>
class (Functor t, Foldable t) => Traversable t where
  traverse :: Applicative k => (a -> k b) -> t a -> k (t b)
</code></pre>

---

## Traversable

<pre style="background-color: black; color: white">
> :type traverse . traverse
traverse . traverse
  :: (Applicative f, Traversable t1, Traversable t2) =>
     (a -> f b) -> t1 (t2 a) -> f (t1 (t2 b))
</pre>

---

## Traversable

<pre><code class="language-haskell hljs" data-trim data-noescape>
traverse ::
  (Traversable t, Applicative k) =>
  (a -> k b) -> t a -> k (t b)
</code></pre>

<pre style="background-color: black; color: white">
> :type traverse . traverse
traverse . traverse
  :: (Applicative k, Traversable t1, Traversable t2) =>
     (a -> f b) -> <mark>t1 (t2</mark> a) -> k (<mark>t1 (t2</mark> b))
</pre>

---

## Traversable

* The composition of two `Traversable` instances is another `Traversable`
* `Traversable` is **closed under composition**

<pre style="background-color: black; color: white">
> :type traverse . traverse
traverse . traverse
  :: (Applicative k, Traversable t1, Traversable t2) =>
     (a -> f b) -> <mark>t1 (t2</mark> a) -> k (<mark>t1 (t2</mark> b))
</pre>

---

## Traversable

###### Therefore

* `List` is `Traversable`
* `Optional` is `Traversable`
* &#8756; if I have a list of optional, it **must** be `Traversable`
* This reasoning will work for any two (or more) traversables &mdash; try some

<pre><code class="language-haskell hljs" data-trim data-noescape>
-- try it!
data ListOptional a = ListOptional (List (Optional a))

instance Foldable ListOptional where -- todo
instance Traversable ListOptional where -- todo
</code></pre>
