## Profunctor { data-transition="fade none" }

---

## Profunctor

* `Profunctor` is a binary functor
  * contravariant in one position
  * covariant in the other position
* That is exactly what `(->)` is!

---

## Profunctor

###### type class

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p where
  dimap :: (a -> b) -> (c -> d) -> p b c -> p a d
</code></pre>

---

## Profunctor

* Similar to `Functor`, `Profunctor` sits at the top of a hierarchy
* It maximises on instances, opposed to derived operations
* What are some instances?

---

## Profunctor

###### Exercises

<div style="font-size:xx-large">
  Note: lots of type variables
</div>

<pre><code class="language-haskell hljs" data-trim data-noescape>
class Profunctor p where
  dimap :: (a -> b) -> (c -> d) -> p b c -> p a d

instance Profunctor (->) where -- todo

data ReaderT k a b = ReaderT (a -> k b)

instance Monad k => Profunctor (ReaderT k) where -- todo
</code></pre>


