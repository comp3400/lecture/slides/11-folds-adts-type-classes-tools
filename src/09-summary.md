## Brief Summary { data-transition="fade none" }

* We have recently seen
  * problems associated with updating nested immutable data types
  * the *"view/update problem"*
  * a type class `Profunctor` defined over binary functors
* `Profunctor` is one abstraction that will assist in resolving the view/update problem

---

## Stay tuned

